from scipy.stats import gamma

vals = gamma.pdf([x/20 for x in range(150)], 1.9)

print("int lum[] = {")
print(", ".join([str(int(100 * x // max(vals))) for x in vals]))
print("};")

#include "FastLED.h"

#define NUM_LEDS 50
#define DATA_PIN 13
#define CLOCK_PIN 11

#define REGRESSION 1
#define PROBABILITY 8

CRGB leds[NUM_LEDS];

uint8_t lum[] = {0, 17, 30, 42, 52, 60, 67, 74, 79, 84, 87, 91, 93, 95, 97, 98, 99, 99, 100, 99, 99, 98, 
                 98, 97, 95, 94, 93, 91, 90, 88, 86, 85, 83, 81, 79, 77, 75, 73, 72, 70, 68, 66, 64, 62, 
                 60, 59, 57, 55, 53, 52, 50, 49, 47, 45, 44, 42, 41, 40, 38, 37, 36, 34, 33, 32, 31, 30, 
                 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 20, 19, 18, 17, 17, 16, 15, 15, 14, 14, 13, 13, 
                 12, 12, 11, 11, 10, 10, 9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 6, 5, 5, 5, 5, 4, 4, 4, 4, 
                 4, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
                 1, 1, 1, 1, 1, 0};
int lum_count;

struct led_state {
  int state;
  CRGB hue;
} led_states[NUM_LEDS];

void setup() {
  int i;
  FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
  lum_count = sizeof(lum) / sizeof(lum[0]);
  for (i = 0; i < NUM_LEDS; i++) {
    led_states[i].state = -1;
  }
}

void loop() {
  int led = random(NUM_LEDS * PROBABILITY);
  int i; 
  
  if (led < NUM_LEDS && led_states[led].state == -1) {
    led_states[led].hue.setHue(random(360));
    led_states[led].state = 0;
  }
  
  for (i = 0; i < NUM_LEDS; i++) {
    if (led_states[i].state >= 0) {
      if (led_states[i].state < lum_count) {
        leds[i] = led_states[i].hue % lum[led_states[i].state];
        led_states[i].state++;
      } else {
        led_states[i].state = -1;
      }
    }
  }
  
  FastLED.show(); 
  delay(5);
}
